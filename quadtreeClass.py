#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
"""TEXTFILE : QuadTree: generalite et application sur les images"""
# ==============================================================================
__author__  = "Authors: Noumbissi Moise, Pons Jean-Baptiste, Zidane Zinedine"
__version__ = "version: 10"
__date__    = "date: 1998-07-12"
__usage__   = """
Note : Enter informations requested when prompt <> is displayed""" 
# ==============================================================================
print("%s\n%s\n%s\n%s\n%s%s\n%s" % ('='*80, __author__,__date__, __version__, __doc__, __usage__, '='*80))
#==========================================================================================================================

from numpy import *
from PIL import Image
import sys

#==========================================================================================================================
# structure de QuadTree générique

class QuadTree:

  def __init__(self, racine, fils_liste):
    self.listeFils=fils_liste
    self.root=racine
    
  def inclure_fils(self,qt):
    self.listeFils=self.listeFils+[qt]


#==========================================================================================================================
# structure d'un noeud
class node:

  def __init__(self, info, profondeur):
    self.data=info
    self.deep=profondeur


#==========================================================================================================================
# application de la classe QuadTree à la lecture et génération d'image
class picture_QuadTree(QuadTree):

  def __init__(self, racine, fils_liste, coordonnee):
    QuadTree.__init__(self,node([0, coordonnee],racine.deep),fils_liste)

  # fonction construisant par récurrence le quadtree à partir de sa matrice associée M
  def QT_Pict_Generator(self, M, prof_max,eps):
    self.root.data[0] = mean(M[self.x0():self.x0()+self.taille(),self.y0():self.y0()+self.taille()])
    moyenne = self.moyenne()
    supi=eps
    infi=1
    Bool = True
    if(eps>0):
      supi = argmax(moyenne*ones((self.taille(),self.taille())) - M[self.x0():self.x0()+self.taille(),self.y0():self.y0()+self.taille()])
      infi = argmin(moyenne*ones((self.taille(),self.taille())) - M[self.x0():self.x0()+self.taille(),self.y0():self.y0()+self.taille()])
    else:
      Bool = any(moyenne*ones((self.taille(),self.taille())) - M[self.x0():self.x0()+self.taille(),self.y0():self.y0()+self.taille()])
    if self.root.deep<=prof_max and abs(supi-infi) > eps and Bool == True :
      liste_fils = self.generation_fils_quadtree(M)
      for _q in liste_fils:
        _q.QT_Pict_Generator(M, prof_max,eps)
        self.inclure_fils(_q)

  # fonction retournant la taille de la matrice associee au quadtree
  def taille(self):
    return self.root.data[1][2]

  # fonction retournant l abscisse de l origine du quadtree
  def x0(self):
    return int(self.root.data[1][0])

  # fonction retournant l ordonnee de l origine du quadtree
  def y0(self):
    return int(self.root.data[1][1])

  # fonction retournant la moyenne de la matrice associee au quadtree
  def moyenne(self):
    return self.root.data[0]

  # fonction generant 4-uplet de fils du QuadTree (correspondant chacun à un quart de l image
  def generation_fils_quadtree(self, M):
    siz=self.taille()
    x0=self.x0()
    y0=self.y0()
    qt1=picture_QuadTree(node(-1,self.root.deep+1),[], (x0,y0,siz/2)) 
    qt2=picture_QuadTree(node(-1,self.root.deep+1),[], (x0,y0+siz/2,siz/2)) 
    qt3=picture_QuadTree(node(-1,self.root.deep+1),[], (x0+siz/2, y0+siz/2, siz/2)) 
    qt4=picture_QuadTree(node(-1,self.root.deep+1),[], (x0+siz/2, y0, siz/2)) 
    return (qt1,qt2,qt3,qt4)
        
  # fonction effectuant la moyenne des valeurs de la matrices associée au QuadTree
  def matrix_mean(self, M):
    s=0
    taille=len(M)
    for i in range(taille):
      s=s+sum(M[i])
    return s/(taille*taille)
 
  # fonction affichant un QuadTree
  def Affiche(self):
    print("profondeur %s de taille = %s et couleur = %s et coord = %s" % (self.root.deep, self.taille, self.moyenne(), self.coord))
 
#---------------------------------------------------------------------------------------------------------------------------
    
  # fonction generant les sous matrices de "l'image"
  def generation_fils_image(self,moyen,Matrix,k):
    if k>0: # reduction resolution
      x0=self.x0()//2**k
      y0=self.y0()//2**k
    else: # agrandissement
      x0=self.x0()/2**k
      y0=self.y0()/2**k
    taille=self.taille()/2**k
    Matrix[x0:(x0+taille),y0:(y0+taille)]=moyen*ones((taille,taille))


  # fonction construisant par récurrence l'image à partir du Quadtree
  def PictQtGenerator(self, Matrix,k):
    moyenne=self.moyenne()
    if len(self.listeFils)!=0:
      for _q in self.listeFils:
        _q.PictQtGenerator(Matrix,k)
    else:
       self.generation_fils_image(self.moyenne(),Matrix,k)
      

#==========================================================================================================================
#
## fonction de pré/post traitement de Quadtree et de matrices de données d'images
#

# fonction retournant la matrice de taille la puissance de 2 juste superieur a la taille de la matrice M
def arrange_matrice(M):
  if len(M[0])!=len(M[0][0]) or 2**(ceil(log(len(M[0]))/log(2)))-len(M[0])!=0:
    t=max(len(M[0]),len(M[0][0]))
    alpha=ceil(log(t)/log(2))
    m=[]
    for k in range(len(M)):
      matrice=255*ones((2**alpha,2**alpha))
      matrice[0:len(M[0]),0:len(M[0][0])]=M[k]
      m+=[matrice]
    return m
  return M

# fonctions générant la matrice associée à une image
def convert_picture_matrix(picture):
  im = Image.open(picture)
  if(im.mode != 'RGB'):
    im= im.convert('RGB')
  x, y = im.size
  data = list(im.getdata())
  # utilisation de numpy pour passer de la liste des valeurs renvoyees par getdata a une matrice
  r=asarray(data)
  if len(r[0])==3:
    decision = input("<> Votre image est en coulour; desirez-vous l'afficher en noir et blanc? oui (o) / non (n): ")
    while decision != 'o' and decision != 'n':
      decision = input("<> Entrez o pour oui ou n pour non: ")
  m1,m2,m3=asarray([r[i][0] for i in range(int(x*y))]),asarray([r[i][1] for i in range(int(x*y))]),asarray([r[i][2] for i in range(int(x*y))])
  if (any(m1-m2)==False and any(m1-m3)==False) or decision == 'o': # matrice pour un affichage en noir et blanc
    m=[m1]
  else: # liste des matrices pour un affichage en couleur
    m=[m1,m2,m3]
  # on transforme liste de données en matrice numpy
  for k in range(len(m)):
    m[k]=m[k].reshape(y,x)
  return m

# fonctions générant l'image associée à une matricejs2_image.save(’C:/FichiersTPImages/new.jpg’)
def convert_matrix_picture(matrix):
  IM=[]
  for k in range(len(matrix)):
    m = matrix[k].astype('uint8')
    im = Image.fromarray(m) # monochromatic image
    IM+=[im]
  if(len(IM)==1):
    imrgb = Image.merge('RGB', (im,im,im)) # color image
  else:
    imrgb = Image.merge('RGB', (IM[0],IM[1],IM[2])) # color image
  imrgb.show()
  imrgb.save('img2.jpg')

#==========================================================================================================================
#
## fonctions de lecture et d ecriture de fichiers txt afin de stocker un quadtree
#


# fonction ecrivant un quadtree dans un fichier txt
# l ecriture d un quadtree aura la forme suivante: [ data[0], (x0,y0), [liste des quadtree fils]]

def str_QT(QT):
  if len(QT.listeFils)==0:
    qt= "[%s,(%s,%s,%s),[]]" % (QT.moyenne(),QT.x0(),QT.y0(),QT.taille())
  else:
    qt= "[%s,(%s,%s,%s),[%s,%s,%s,%s]]" % (QT.x0(),QT.y0(),QT.taille(),str_QT(QT.listeFils[0]),str_QT(QT.listeFils[1]),str_QT(QT.listeFils[2]),str_QT(QT.listeFils[3]))
  return qt

def write_QT(QT):
  with open('quadtree.txt', 'w') as file:
    #print(str_QT(QT))
    file.write(str_QT(QT).strip('\\'))


  
# fonction generant un quadtree a partir de son ecriture dans un fichier txt

##def r_QT(qt, prof):
##  
##
##  
##def read_QT(file_name):
##  with open(file_name,'r') as file:
##    _qt = file.read().split(',')
##    _qt0 = _qt[0].strip('[')
##    _qt1 = _qt[1].strip('(')
##    _qt3 = _qt[3].strip(')')
##    qt = pict_QuadTree(node(int(_qt0),0),[],(int(_qt1),int(_qt2),int(_qt3)))
##    if _qt[4]!='[]':
##      qt.listeFils=r_QT(_qt[3:])
##    return qt
##    # on obtient alors de la structure 3 élém:
##    # _qt[0]: valeur moyenne,
##    # _qt[1]: coord x0
##    # _qt[2]: coord y0
##    # _qt[3:]: liste de fils => si _qt[3] == [], liste vide, sinon on plonge
##
##    
##    # par recurrence, generer fils
      
  

#==========================================================================================================================
## fonctions générales de construction de QuadTree et d'images

def getQuadTreeFromPicture(picture, prof_max, eps):
  matrice = convert_picture_matrix(picture)
  l1=len(matrice[0]);l2=len(matrice[0][0])
  matrice = arrange_matrice(matrice)
  # convert_matrix_picture(matrice)
  QT=[]
  for mat in matrice:
    qt = picture_QuadTree(node([],0),[],(0,0,len(mat)))
    qt.QT_Pict_Generator(mat, prof_max,eps)
    QT += [qt]
  return QT

def getTaillePicture(picture):
  im = Image.open(picture)
  return im.size


def getPictureFromQuadTree(QT,l1,l2):
  M=[]
  _sup = max(l1,l2)
  k = (input("<> Resolution de votre image? "))
  if k=='':
    k=0
  while 2**(int(k)) > _sup:
    k = (input("<> La reduction de resolution est trop importante, veuillez entrer une valeur plus petite: "))
    if k=='':
      k=0
  else:
    k=int(k)
  for _qt in QT:
    Matrix = ones((QT[0].taille()/2**k,QT[0].taille()/2**k))
    _qt.PictQtGenerator(Matrix,k)
    Matrix=Matrix[0:l2/2**k,0:l1/2**k]
    M+=[Matrix]
  return M
  

#==========================================================================================================================

def main():
  while True:
    picture = input("<> Entrez le nom de l'image: ")
    if picture != '':
      prof_max = (input("<> Entrez la profondeur max souhaitee: "))
      if prof_max == '':
        prof_max = Infinity
      else:
        prof_max = int(prof_max)
      eps = (input("<> Entrez le coeff d'approximation de qualité souhaite: "))
      if eps =='':
        eps=0
      else:
        eps=int(eps)
      QT = getQuadTreeFromPicture(picture, prof_max, eps)
      l1,l2= getTaillePicture(picture)
      M = getPictureFromQuadTree(QT, l1, l2)
      convert_matrix_picture(M)
    else: break

    
#---------------------------------------------------------------------------------------------------------------------------------------------------------
if __name__=='__main__' :
  main()
#==========================================================================================================================
